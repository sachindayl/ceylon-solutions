package com.sachinda.ceylonsolutionsproject.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.sachinda.ceylonsolutionsproject.database.entities.PostEntity

@Dao
interface PostsDao {
    @Insert
    suspend fun insert(vararg post: PostEntity)

    @Query("SELECT * FROM posts")
    suspend fun getPosts(): List<PostEntity>

    @Query("DELETE FROM posts")
    suspend fun deleteAll()
}