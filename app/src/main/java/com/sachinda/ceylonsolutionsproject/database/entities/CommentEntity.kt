package com.sachinda.ceylonsolutionsproject.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "comments")
data class CommentEntity(
    @PrimaryKey(autoGenerate = false) @ColumnInfo(name = "comment_id") val commentId: Int,
    @ColumnInfo(name = "post_id") val postId: Int,
    @ColumnInfo(name = "post_name") val name: String,
    val email: String,
    val body: String
)