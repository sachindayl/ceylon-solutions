package com.sachinda.ceylonsolutionsproject.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "companies")
data class CompanyEntity(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "company_id") val companyId: Int,
    @ColumnInfo(name = "company_name") val name: String,
    @ColumnInfo(name = "catch_phrase") val catchPhrase: String,
    val bs: String
)