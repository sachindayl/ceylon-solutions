package com.sachinda.ceylonsolutionsproject.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.sachinda.ceylonsolutionsproject.database.dao.CommentsDao
import com.sachinda.ceylonsolutionsproject.database.dao.PostsDao
import com.sachinda.ceylonsolutionsproject.database.dao.UsersDao
import com.sachinda.ceylonsolutionsproject.database.entities.CommentEntity
import com.sachinda.ceylonsolutionsproject.database.entities.PostEntity
import com.sachinda.ceylonsolutionsproject.database.entities.UserEntity

@Database(entities = [PostEntity::class, UserEntity::class, CommentEntity::class], version = 7, exportSchema = false)
abstract class CSDatabase : RoomDatabase() {

    abstract val postsDao: PostsDao
    abstract val usersDao: UsersDao
    abstract val commentsDao: CommentsDao

    companion object {

        @Volatile
        private var INSTANCE: CSDatabase? = null

        fun getInstance(context: Context): CSDatabase {
            synchronized(this) {
                var instance = INSTANCE

                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        CSDatabase::class.java,
                        "ceylon_solutions_database"
                    )
                        .fallbackToDestructiveMigration()
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}