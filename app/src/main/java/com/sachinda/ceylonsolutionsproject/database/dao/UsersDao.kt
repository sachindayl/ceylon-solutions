package com.sachinda.ceylonsolutionsproject.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.sachinda.ceylonsolutionsproject.database.entities.UserEntity

@Dao
interface UsersDao {
    @Insert
    suspend fun insert(vararg user: UserEntity)

    @Query("SELECT * FROM users")
    suspend fun getUsers():List<UserEntity>

    @Query("DELETE FROM users")
    suspend fun deleteAll()
}