package com.sachinda.ceylonsolutionsproject.database.entities

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "users")
data class UserEntity(
    @PrimaryKey(autoGenerate = false) @ColumnInfo(name = "user_id") val userId: Int,
    @ColumnInfo(name = "user_name") val name: String,
    @ColumnInfo(name = "user_username") val username: String,
    val email: String,
    val phone: String,
    val website: String,
    @Embedded val address: AddressEntity,
    @Embedded val company: CompanyEntity
)