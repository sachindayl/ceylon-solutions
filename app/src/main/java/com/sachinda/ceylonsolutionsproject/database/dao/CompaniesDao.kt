package com.sachinda.ceylonsolutionsproject.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.sachinda.ceylonsolutionsproject.database.entities.CompanyEntity

@Dao
interface CompaniesDao {
    @Insert
    fun insert(company: CompanyEntity): Long

    @Query("DELETE FROM companies")
    fun deleteAll()
}