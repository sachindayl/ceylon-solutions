package com.sachinda.ceylonsolutionsproject.database

import com.sachinda.ceylonsolutionsproject.database.dao.CommentsDao
import com.sachinda.ceylonsolutionsproject.database.dao.PostsDao
import com.sachinda.ceylonsolutionsproject.database.dao.UsersDao

data class CSDataSource(
    val postsDao: PostsDao,
    val usersDao: UsersDao,
    val commentsDao: CommentsDao
)