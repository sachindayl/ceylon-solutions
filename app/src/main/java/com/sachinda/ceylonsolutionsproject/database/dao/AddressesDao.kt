package com.sachinda.ceylonsolutionsproject.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.sachinda.ceylonsolutionsproject.database.entities.AddressEntity

@Dao
interface AddressesDao {
    @Insert
    fun insert( address: AddressEntity): Long

    @Query("DELETE FROM addresses")
    fun deleteAll()
}