package com.sachinda.ceylonsolutionsproject.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.sachinda.ceylonsolutionsproject.database.entities.CommentEntity

@Dao
interface CommentsDao {
    @Insert
    suspend fun insert(vararg comments: CommentEntity)

    @Query("SELECT * FROM comments")
    suspend fun getComments():List<CommentEntity>

    @Query("DELETE FROM comments")
    suspend fun deleteAll()
}