package com.sachinda.ceylonsolutionsproject.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "posts")
data class PostEntity(
    @PrimaryKey(autoGenerate = false) @ColumnInfo(name = "post_id") val postId: Int,
    @ColumnInfo(name = "user_id") val userId: Int,
    val title: String,
    val body: String
)