package com.sachinda.ceylonsolutionsproject

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.sachinda.ceylonsolutionsproject.databinding.ActivityMainBinding
import com.sachinda.ceylonsolutionsproject.models.BackButtonCallback
import com.sachinda.ceylonsolutionsproject.views.HomeFragment
import com.sachinda.ceylonsolutionsproject.views.HomeFragment.Companion.HOME_FRAGMENT_SCREEN

class MainActivity : AppCompatActivity(), BackButtonCallback {

    private var _binding: ActivityMainBinding? = null
    private val binding get() = _binding!!
    private var mActivateBackButton: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if (savedInstanceState != null) {
            return
        }
        supportFragmentManager
            .beginTransaction()
            .add(R.id.fragment_container, HomeFragment.newInstance(), HOME_FRAGMENT_SCREEN)
            .commit()
    }

    override fun isBackButtonActivated(activated: Boolean) {
        mActivateBackButton = activated
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        try {
            when (item.itemId) {
                android.R.id.home ->
                    if (mActivateBackButton) {
                        supportFragmentManager.popBackStack()
                    }
            }
        } catch (e: Exception) {
            Log.e("MainActivity","Back press runtime error", e)
        }
        return super.onOptionsItemSelected(item)
    }

}
