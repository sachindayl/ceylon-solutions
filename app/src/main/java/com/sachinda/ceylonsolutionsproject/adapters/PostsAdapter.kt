package com.sachinda.ceylonsolutionsproject.adapters

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sachinda.ceylonsolutionsproject.MainActivity
import com.sachinda.ceylonsolutionsproject.R
import com.sachinda.ceylonsolutionsproject.models.Post
import com.sachinda.ceylonsolutionsproject.models.User
import com.sachinda.ceylonsolutionsproject.views.PostFragment
import com.sachinda.ceylonsolutionsproject.views.PostFragment.Companion.POST_SCREEN
import kotlinx.android.synthetic.main.posts_list_item.view.*

class PostsAdapter(
    private var itemList: List<Post>,
    private var userList: List<User>,
    private val context: Context
) :
    RecyclerView.Adapter<PostsHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostsHolder {
        val layoutInflater = LayoutInflater.from(context)
        return PostsHolder(
            layoutInflater.inflate(R.layout.posts_list_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: PostsHolder, position: Int) {
        val userId = itemList[position].userId
        val user = userList.find { user -> user.id == userId }
        val url = "https://api.adorable.io/avatars/64/${user?.email}"
        Glide.with(context).load(url).placeholder(R.drawable.ic_launcher_foreground)
            .into(holder.ivItemImage!!)
        holder.tvItemTitle?.text = itemList[position].title
    }

    fun setData(postData: List<Post>, userData: List<User>) {
        this.itemList = postData
        this.userList = userData
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return itemList.size
    }
}

class PostsHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val mainActivity = view.context as MainActivity
    val tvItemTitle: TextView? = view.title_tv
    val ivItemImage: ImageView? = view.itemImage_iv
    private val bundle = Bundle()

    init {
        try {
            view.setOnClickListener {
                val postFragment = PostFragment.newInstance()
                bundle.putInt("postIndex", adapterPosition)
                postFragment.arguments = bundle
                mainActivity.supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment_container, postFragment)
                    .addToBackStack(POST_SCREEN)
                    .commit()
            }
        } catch (e: IndexOutOfBoundsException) {
            e.printStackTrace()
        }

    }
}
