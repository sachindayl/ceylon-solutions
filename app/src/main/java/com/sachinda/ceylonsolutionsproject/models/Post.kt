package com.sachinda.ceylonsolutionsproject.models

import java.io.Serializable

data class Post(
    val id: Int = 0,
    val userId: Int,
    val title: String?,
    val body: String?
) : Serializable