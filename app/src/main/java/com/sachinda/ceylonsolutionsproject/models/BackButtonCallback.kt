package com.sachinda.ceylonsolutionsproject.models

interface BackButtonCallback {
    fun isBackButtonActivated(activated: Boolean)
}