package com.sachinda.ceylonsolutionsproject.models

import java.io.Serializable

data class Company(
    val name: String,
    val catchPhrase: String,
    val bs: String
): Serializable