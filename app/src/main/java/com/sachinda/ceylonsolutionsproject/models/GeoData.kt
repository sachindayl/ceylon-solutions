package com.sachinda.ceylonsolutionsproject.models

import java.io.Serializable

data class GeoData(
    val lat: String,
    val lng: String
):Serializable