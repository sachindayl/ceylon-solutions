package com.sachinda.ceylonsolutionsproject.viewmodels

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.sachinda.ceylonsolutionsproject.database.CSDataSource
import com.sachinda.ceylonsolutionsproject.database.entities.*
import com.sachinda.ceylonsolutionsproject.models.*
import com.sachinda.ceylonsolutionsproject.services.JsonPlaceholderAPI
import com.sachinda.ceylonsolutionsproject.services.NetworkConnectivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.SocketTimeoutException

class PostsViewModel(
    private val postsApi: JsonPlaceholderAPI,
    private val csDataSource: CSDataSource,
    application: Application
) : AndroidViewModel(application) {
    lateinit var postsList: List<Post>
    private val _userList: MutableLiveData<List<User>> = MutableLiveData()
    val userList: LiveData<List<User>> = _userList
    private val _commentsCount: MutableLiveData<Int> = MutableLiveData()
    val commentsCount: LiveData<Int> = _commentsCount
    private val _isPostAdded: MutableLiveData<Boolean> = MutableLiveData()
    val isPostAdded: LiveData<Boolean> = _isPostAdded

    fun retrievePostsAndUsers(context: Context, networkConnectivity: NetworkConnectivity) {
        viewModelScope.launch {
            try {
                if (networkConnectivity.isOnline(context)) {
                    postsList = postsApi.getPosts()
                    _userList.value = postsApi.getUsers()
                        csDataSource.postsDao.deleteAll()
                        csDataSource.usersDao.deleteAll()
                        postsList.map {
                            csDataSource.postsDao.insert(
                                PostEntity(
                                    it.id,
                                    it.userId,
                                    it.title!!,
                                    it.body!!
                                )
                            )
                    }

                    val userList = _userList.value
                        userList?.map {
                            csDataSource.usersDao.insert(
                                UserEntity(
                                    it.id,
                                    it.name,
                                    it.username,
                                    it.email,
                                    it.phone,
                                    it.website,
                                    AddressEntity(
                                        it.id,
                                        it.address.street,
                                        it.address.suite,
                                        it.address.city,
                                        it.address.zipcode,
                                        it.address.geo.lat,
                                        it.address.geo.lng
                                    ),
                                    CompanyEntity(
                                        it.id,
                                        it.company.name,
                                        it.company.catchPhrase,
                                        it.company.bs
                                    )
                                )
                            )
                    }

                } else {
                    postsList = csDataSource.postsDao.getPosts().map {
                        Post(
                            it.postId,
                            it.userId,
                            it.title,
                            it.body
                        )
                    }

                    val users = async {
                        csDataSource.usersDao.getUsers().map {
                            User(
                                it.userId,
                                it.name,
                                it.username,
                                it.email,
                                it.phone,
                                it.website,
                                Address(
                                    it.address.street,
                                    it.address.suite,
                                    it.address.city,
                                    it.address.zipcode,
                                    GeoData(it.address.lat, it.address.lng)
                                ),
                                Company(
                                    it.company.name,
                                    it.company.catchPhrase,
                                    it.company.bs
                                )
                            )
                        }
                    }

                    _userList.value = users.await()

                }
            } catch (e: SocketTimeoutException) {
                e.printStackTrace()
            } catch (e: Throwable) {
                e.printStackTrace()
            }

        }
    }

    fun retrieveCommentsCount(
        postId: Int,
        networkConnectivity: NetworkConnectivity,
        context: Context
    ) {
        viewModelScope.launch {
            try {
                    val comments: List<Comment>
                    if (networkConnectivity.isOnline(context)) {
                        csDataSource.commentsDao.deleteAll()
                        comments = postsApi.getComments()
                        comments.map {
                            csDataSource.commentsDao.insert(
                                CommentEntity(
                                    it.id,
                                    it.postId,
                                    it.name,
                                    it.email,
                                    it.body
                                )
                            )
                        }
                    } else {
                        comments = csDataSource.commentsDao.getComments().map {
                            Comment(
                                it.commentId,
                                it.postId,
                                it.name,
                                it.email,
                                it.body
                            )
                        }
                    }

                val count = withContext(Dispatchers.IO) {
                    var value = 0
                    comments.forEach { comment ->
                        if (comment.postId == postId) {
                            value++
                        }
                    }
                    return@withContext value
                }

                _commentsCount.value = count
            } catch (e: SocketTimeoutException) {
                e.printStackTrace()
            } catch (e: Throwable) {
                e.printStackTrace()
            }

        }
    }

    fun addPost(post: Post) = viewModelScope.launch {
        try {
            val result = postsApi.addPost(post)
            if (result.isSuccessful) {
                _isPostAdded.value = true
            }
        } catch (e: SocketTimeoutException) {
            _isPostAdded.value = false
            e.printStackTrace()
        } catch (e: Throwable) {
            _isPostAdded.value = false
            e.printStackTrace()
        }
    }

    fun getPost(index: Int): Post? {
        return postsList[index]
    }

    fun getUserById(userId: Int): User? {
        userList.value?.forEach { user ->
            if (user.id == userId) {
                return user
            }
        }
        return null
    }
}