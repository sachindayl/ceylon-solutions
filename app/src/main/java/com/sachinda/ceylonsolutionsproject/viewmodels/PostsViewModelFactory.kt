package com.sachinda.ceylonsolutionsproject.viewmodels

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.sachinda.ceylonsolutionsproject.database.CSDataSource
import com.sachinda.ceylonsolutionsproject.services.JsonPlaceholderAPI

class PostsViewModelFactory(
    private val postsApi: JsonPlaceholderAPI,
    private val csDataSource: CSDataSource,
    private val application: Application
): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PostsViewModel(postsApi, csDataSource, application) as T
    }
}