package com.sachinda.ceylonsolutionsproject.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.sachinda.ceylonsolutionsproject.MainActivity
import com.sachinda.ceylonsolutionsproject.R
import com.sachinda.ceylonsolutionsproject.adapters.PostsAdapter
import com.sachinda.ceylonsolutionsproject.database.CSDataSource
import com.sachinda.ceylonsolutionsproject.database.CSDatabase
import com.sachinda.ceylonsolutionsproject.databinding.FragmentHomeBinding
import com.sachinda.ceylonsolutionsproject.models.BackButtonCallback
import com.sachinda.ceylonsolutionsproject.services.JsonPlaceholderAPI
import com.sachinda.ceylonsolutionsproject.services.NetworkConnectivity
import com.sachinda.ceylonsolutionsproject.services.RetrofitService
import com.sachinda.ceylonsolutionsproject.viewmodels.PostsViewModel
import com.sachinda.ceylonsolutionsproject.viewmodels.PostsViewModelFactory
import com.sachinda.ceylonsolutionsproject.views.AddPostFragment.Companion.ADD_POST_SCREEN

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private lateinit var vm: PostsViewModel
    private lateinit var adapter: PostsAdapter

    companion object {
        const val HOME_FRAGMENT_SCREEN = "homeFragment"
        fun newInstance() = HomeFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val actionBar = (requireActivity() as MainActivity).supportActionBar
        actionBar?.title = "Ceylon Solutions"
        actionBar?.setDisplayHomeAsUpEnabled(false)
        val mBackButtonActivated: BackButtonCallback? = activity as MainActivity
        mBackButtonActivated?.isBackButtonActivated(false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val api = RetrofitService.getBuilder().create(JsonPlaceholderAPI::class.java)
        val application = requireActivity().application
        val dataSource = CSDataSource(
            CSDatabase.getInstance(application).postsDao,
            CSDatabase.getInstance(application).usersDao,
            CSDatabase.getInstance(application).commentsDao
        )
        val viewModelFactory = PostsViewModelFactory(api, dataSource, application)
        vm = ViewModelProvider(requireActivity(), viewModelFactory).get(PostsViewModel::class.java)
        adapter = PostsAdapter(arrayListOf(), arrayListOf(), requireContext())
        binding.postsRv.layoutManager = LinearLayoutManager(requireActivity())
        binding.postsRv.adapter = adapter
        vm.retrievePostsAndUsers(this.requireContext(), NetworkConnectivity())
        vm.userList.observe(this.viewLifecycleOwner, Observer { users ->
            adapter.setData(vm.postsList, users)
        })
        binding.floatingActionButton.setOnClickListener {
            requireActivity().supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, AddPostFragment.newInstance())
                .addToBackStack(ADD_POST_SCREEN)
                .commit()
        }
    }
}
