package com.sachinda.ceylonsolutionsproject.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.sachinda.ceylonsolutionsproject.MainActivity
import com.sachinda.ceylonsolutionsproject.R
import com.sachinda.ceylonsolutionsproject.database.CSDataSource
import com.sachinda.ceylonsolutionsproject.database.CSDatabase
import com.sachinda.ceylonsolutionsproject.databinding.PostFragmentBinding
import com.sachinda.ceylonsolutionsproject.models.BackButtonCallback
import com.sachinda.ceylonsolutionsproject.services.JsonPlaceholderAPI
import com.sachinda.ceylonsolutionsproject.services.NetworkConnectivity
import com.sachinda.ceylonsolutionsproject.services.RetrofitService
import com.sachinda.ceylonsolutionsproject.viewmodels.PostsViewModel
import com.sachinda.ceylonsolutionsproject.viewmodels.PostsViewModelFactory
import com.sachinda.ceylonsolutionsproject.views.UserFragment.Companion.USER_SCREEN

class PostFragment : Fragment() {
    private var _binding: PostFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel: PostsViewModel
    private lateinit var bundle: Bundle

    companion object {
        const val POST_SCREEN = "Post screen"
        fun newInstance() = PostFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = PostFragmentBinding.inflate(layoutInflater, container, false)
        (requireActivity() as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val mBackButtonActivated: BackButtonCallback? = activity as MainActivity
        mBackButtonActivated?.isBackButtonActivated(true)
        bundle = this.requireArguments()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val api = RetrofitService.getBuilder().create(JsonPlaceholderAPI::class.java)
        val application = requireActivity().application
        val dataSource = CSDataSource(
            CSDatabase.getInstance(application).postsDao,
            CSDatabase.getInstance(application).usersDao,
            CSDatabase.getInstance(application).commentsDao
        )
        val viewModelFactory = PostsViewModelFactory(api, dataSource, application)
        viewModel = ViewModelProvider(requireActivity(), viewModelFactory).get(PostsViewModel::class.java)
        val postIndex = bundle.getInt("postIndex")
        val postId = viewModel.getPost(postIndex)?.id
        val userId = viewModel.getPost(postIndex)?.userId
        postId?.let { viewModel.retrieveCommentsCount(it, NetworkConnectivity(), this.requireContext()) }
        val user = userId?.let { viewModel.getUserById(it) }
        user?.let { userValue -> binding.usernameBtn.text = userValue.username }
        viewModel.commentsCount.observe(viewLifecycleOwner, Observer {
            binding.numberCommentsTv.text = getString(R.string.comments, it.toString())
        })
        binding.postTitleTv.text = viewModel.getPost(postIndex)?.title
        binding.postBodyTv.text = viewModel.getPost(postIndex)?.body
        binding.usernameBtn.setOnClickListener {
            val bundle = Bundle()
            userId?.let { id -> bundle.putInt("userId", id) }
            val userFragment = UserFragment.newInstance()
            userFragment.arguments = bundle
            requireActivity().supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, userFragment)
                .addToBackStack(USER_SCREEN)
                .commit()
        }
    }

}
