package com.sachinda.ceylonsolutionsproject.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.sachinda.ceylonsolutionsproject.MainActivity
import com.sachinda.ceylonsolutionsproject.R
import com.sachinda.ceylonsolutionsproject.database.CSDataSource
import com.sachinda.ceylonsolutionsproject.database.CSDatabase
import com.sachinda.ceylonsolutionsproject.databinding.UserFragmentBinding
import com.sachinda.ceylonsolutionsproject.models.BackButtonCallback
import com.sachinda.ceylonsolutionsproject.services.JsonPlaceholderAPI
import com.sachinda.ceylonsolutionsproject.services.RetrofitService
import com.sachinda.ceylonsolutionsproject.viewmodels.PostsViewModel
import com.sachinda.ceylonsolutionsproject.viewmodels.PostsViewModelFactory


class UserFragment : Fragment(), OnMapReadyCallback {

    companion object {
        const val USER_SCREEN = "user screen"
        fun newInstance() = UserFragment()
    }

    private lateinit var gMap: GoogleMap
    private var _binding: UserFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel: PostsViewModel
    private lateinit var bundle: Bundle
    private lateinit var latLng: LatLng;

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = UserFragmentBinding.inflate(inflater, container, false)
        bundle = this.requireArguments()
        (requireActivity() as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val mBackButtonActivated: BackButtonCallback? = activity as MainActivity
        mBackButtonActivated?.isBackButtonActivated(true)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val api = RetrofitService.getBuilder().create(JsonPlaceholderAPI::class.java)
        val application = requireActivity().application
        val dataSource = CSDataSource(
            CSDatabase.getInstance(application).postsDao,
            CSDatabase.getInstance(application).usersDao,
            CSDatabase.getInstance(application).commentsDao
        )
        val viewModelFactory = PostsViewModelFactory(api, dataSource, application)
        viewModel = ViewModelProvider(requireActivity(), viewModelFactory).get(PostsViewModel::class.java)
        val user = bundle.getInt("userId").let { id -> viewModel.getUserById(id) }
        val mapFragment =
            this.childFragmentManager.findFragmentById(binding.mapOr.id) as SupportMapFragment
        user?.let {
            val url = "https://api.adorable.io/avatars/64/${it.email}"
            Glide.with(requireContext())
                .load(url)
                .placeholder(R.drawable.ic_launcher_foreground)
                .into(binding.userIv)
            binding.userNameTv.text = it.name
            binding.userUsernameTv.text = it.username
            binding.userEmailTv.text = it.email
            binding.userPhoneTv.text = it.phone
            binding.userWebTv.text = it.website
            binding.compNameTv.text = it.company.name
            binding.compBsTv.text = it.company.bs
            binding.compCpTv.text = it.company.catchPhrase
            binding.locStreetTv.text = it.address.street
            binding.locSuiteTv.text = it.address.suite
            binding.locCityTv.text = it.address.city
            binding.locZipTv.text = it.address.zipcode
            val lat = it.address.geo.lat.toDouble()
            val lng = it.address.geo.lng.toDouble()
            latLng = LatLng(lat, lng)
            mapFragment.getMapAsync(this)
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        gMap = googleMap
        gMap.setMinZoomPreference(1.0f)
        gMap.setMaxZoomPreference(20.0f)
        gMap.addMarker(MarkerOptions().position(latLng))
        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 3.0f))
    }
}
