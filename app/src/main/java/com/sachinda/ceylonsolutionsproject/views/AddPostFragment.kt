package com.sachinda.ceylonsolutionsproject.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.sachinda.ceylonsolutionsproject.MainActivity
import com.sachinda.ceylonsolutionsproject.R
import com.sachinda.ceylonsolutionsproject.database.CSDataSource
import com.sachinda.ceylonsolutionsproject.database.CSDatabase
import com.sachinda.ceylonsolutionsproject.databinding.AddPostFragmentBinding
import com.sachinda.ceylonsolutionsproject.models.BackButtonCallback
import com.sachinda.ceylonsolutionsproject.models.Post
import com.sachinda.ceylonsolutionsproject.services.JsonPlaceholderAPI
import com.sachinda.ceylonsolutionsproject.services.RetrofitService
import com.sachinda.ceylonsolutionsproject.viewmodels.PostsViewModel
import com.sachinda.ceylonsolutionsproject.viewmodels.PostsViewModelFactory

class AddPostFragment : Fragment() {
    private var _binding: AddPostFragmentBinding? = null
    private val binding get() = _binding!!

    companion object {
        const val ADD_POST_SCREEN = "Add post"
        fun newInstance() = AddPostFragment()
    }

    private lateinit var viewModel: PostsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = AddPostFragmentBinding.inflate(inflater, container, false)
        (requireActivity() as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val mBackButtonActivated: BackButtonCallback? = activity as MainActivity
        mBackButtonActivated?.isBackButtonActivated(true)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val api = RetrofitService.getBuilder().create(JsonPlaceholderAPI::class.java)
        val application = requireActivity().application
        val dataSource = CSDataSource(
            CSDatabase.getInstance(application).postsDao,
            CSDatabase.getInstance(application).usersDao,
            CSDatabase.getInstance(application).commentsDao
        )
        val viewModelFactory = PostsViewModelFactory(api, dataSource, application)
        viewModel =
            ViewModelProvider(requireActivity(), viewModelFactory).get(PostsViewModel::class.java)

        binding.addBtn.setOnClickListener {
            binding.progressBar.visibility = View.VISIBLE
            viewModel.addPost(
                Post(
                    userId = 1,
                    title = binding.titleEt.text.toString(),
                    body = binding.bodyEt.text.toString()
                )
            )
        }

        viewModel.isPostAdded.observe(viewLifecycleOwner, Observer { isAdded ->
            binding.progressBar.visibility = View.INVISIBLE
            if (isAdded) {
                Toast.makeText(requireActivity(), "Post added!", Toast.LENGTH_SHORT).show()
                requireActivity()
                    .supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, HomeFragment.newInstance())
                    .addToBackStack(ADD_POST_SCREEN)
                    .commit()
            } else {
                Toast.makeText(
                    requireActivity(),
                    "Request failed. Please try again.",
                    Toast.LENGTH_SHORT
                ).show()
            }
        })

    }

}
