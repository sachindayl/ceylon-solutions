package com.sachinda.ceylonsolutionsproject.services

import com.sachinda.ceylonsolutionsproject.models.Comment
import com.sachinda.ceylonsolutionsproject.models.Post
import com.sachinda.ceylonsolutionsproject.models.User
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST

interface JsonPlaceholderAPI {
    @Headers("Content-Type: application/json")
    @GET("/posts")
    suspend fun getPosts(): List<Post>

    @Headers("Content-Type: application/json")
    @GET("/users")
    suspend fun getUsers(): List<User>

    @Headers("Content-Type: application/json")
    @GET("/comments")
    suspend fun getComments(): List<Comment>

    @Headers("Content-Type: application/json")
    @POST("/posts")
    suspend fun addPost(@Body post: Post): Response<ResponseBody>
}