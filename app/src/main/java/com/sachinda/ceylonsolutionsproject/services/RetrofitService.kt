package com.sachinda.ceylonsolutionsproject.services

import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

interface IRetrofitService {
    fun getBuilder(): Retrofit
}

object RetrofitService : IRetrofitService {
    private val gSon = GsonBuilder()
        .setLenient()
        .create()

    override fun getBuilder(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("http://jsonplaceholder.typicode.com")
            .addConverterFactory(GsonConverterFactory.create(gSon))
            .build()
    }

}