package com.sachinda.ceylonsolutionsproject

import android.app.Application
import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.google.gson.GsonBuilder
import com.sachinda.ceylonsolutionsproject.database.CSDataSource
import com.sachinda.ceylonsolutionsproject.database.dao.CommentsDao
import com.sachinda.ceylonsolutionsproject.database.dao.PostsDao
import com.sachinda.ceylonsolutionsproject.database.dao.UsersDao
import com.sachinda.ceylonsolutionsproject.database.entities.*
import com.sachinda.ceylonsolutionsproject.models.Comment
import com.sachinda.ceylonsolutionsproject.models.Post
import com.sachinda.ceylonsolutionsproject.models.User
import com.sachinda.ceylonsolutionsproject.services.JsonPlaceholderAPI
import com.sachinda.ceylonsolutionsproject.services.NetworkConnectivity
import com.sachinda.ceylonsolutionsproject.viewmodels.PostsViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.*
import kotlinx.coroutines.withContext
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import java.io.BufferedReader
import java.io.FileInputStream
import java.io.InputStreamReader


@ExperimentalCoroutinesApi
class JsonPlaceholderServiceTest {
    private val testDispatcher = TestCoroutineDispatcher()
    private val testScope = TestCoroutineScope(testDispatcher)

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var application: Application

    @Mock
    lateinit var context: Context

    @Mock
    lateinit var networkConnectivity: NetworkConnectivity

    @Mock
    lateinit var jsonPlaceholderAPI: JsonPlaceholderAPI

    @Mock
    lateinit var postsDao: PostsDao

    @Mock
    lateinit var usersDao: UsersDao

    @Mock
    lateinit var commentsDao: CommentsDao

    private lateinit var postsViewModel: PostsViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(testDispatcher)

        postsViewModel = PostsViewModel(
            jsonPlaceholderAPI,
            CSDataSource(postsDao, usersDao, commentsDao),
            application
        )
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testScope.cleanupTestCoroutines()
    }

    @Test
    fun testGetEmptyPostAndUserListsOnline() = testScope.runBlockingTest {
        `when`(jsonPlaceholderAPI.getPosts()).thenReturn(listOf())
        `when`(jsonPlaceholderAPI.getUsers()).thenReturn(listOf())
        `when`(networkConnectivity.isOnline(context)).thenReturn(true)
        postsViewModel.retrievePostsAndUsers(context, networkConnectivity)
        verify(jsonPlaceholderAPI, times(1)).getPosts()
        verify(jsonPlaceholderAPI, times(1)).getUsers()
        assertEquals(0, postsViewModel.postsList.size)
        assertEquals(0, postsViewModel.userList.value?.size)
    }

    @Test
    fun testGetEmptyPostAndUserListsOffline() = testScope.runBlockingTest {
        `when`(postsDao.getPosts()).thenReturn(listOf())
        `when`(usersDao.getUsers()).thenReturn(listOf())
        `when`(networkConnectivity.isOnline(context)).thenReturn(false)
        postsViewModel.retrievePostsAndUsers(context, networkConnectivity)
        verify(postsDao, times(1)).getPosts()
        verify(usersDao, times(1)).getUsers()
        assertEquals(0, postsViewModel.postsList.size)
        assertEquals(0, postsViewModel.userList.value?.size)
    }

    @Test
    fun testGetPostAndUserListsOnline() = testScope.runBlockingTest {
        val gSon = GsonBuilder().create()
        val posts = gSon.fromJson(
            readJsonFile("testPosts.json"),
            Array<Post>::class.java
        )
        val users = gSon.fromJson(
            readJsonFile("testUsers.json"),
            Array<User>::class.java
        )
        `when`(jsonPlaceholderAPI.getPosts()).thenReturn(posts.toList())
        `when`(jsonPlaceholderAPI.getUsers()).thenReturn(users.toList())
        `when`(networkConnectivity.isOnline(context)).thenReturn(true)
        postsViewModel.retrievePostsAndUsers(context, networkConnectivity)
        verify(jsonPlaceholderAPI, times(1)).getPosts()
        verify(jsonPlaceholderAPI, times(1)).getUsers()
        assertEquals(2, postsViewModel.postsList.size)
        assertEquals(3, postsViewModel.userList.value?.size)
    }

    @Test
    fun testGetPostAndUserListsOffline() = testScope.runBlockingTest {
        val posts = arrayListOf(
            PostEntity(1, 1, "test", "test"),
            PostEntity(2, 1, "test2", "test2")
        )
        val users = arrayListOf(
            UserEntity(
                1,
                "john",
                "@john",
                "asdjasdk@sada.com",
                "1284721984",
                "asdkas.com",
                AddressEntity(1, "1123 jkasdhsa", "12", "ddas", "31221", "24.212", "-8.414"),
                CompanyEntity(1, "gdfhjjj", "dshfhfdh", "dfh")
            )
        )
        `when`(postsDao.getPosts()).thenReturn(posts)
        `when`(usersDao.getUsers()).thenReturn(users)
        `when`(networkConnectivity.isOnline(context)).thenReturn(false)
        postsViewModel.retrievePostsAndUsers(context, networkConnectivity)
        verify(postsDao, times(1)).getPosts()
        verify(usersDao, times(1)).getUsers()
        assertEquals(2, postsViewModel.postsList.size)
        assertEquals(1, postsViewModel.userList.value?.size)
    }

    @Test
    fun testCommentsCountWhenOnline() = runBlockingTest {
        val gSon = GsonBuilder().create()
        val comments = gSon.fromJson(
            readJsonFile("testComments.json"),
            Array<Comment>::class.java
        )
        `when`(jsonPlaceholderAPI.getComments()).thenReturn(comments.toList())
        `when`(networkConnectivity.isOnline(context)).thenReturn(true)
        withContext(Dispatchers.Unconfined) {
        postsViewModel.retrieveCommentsCount(1, networkConnectivity, context)
        }
        verify(jsonPlaceholderAPI, times(1)).getComments()
        delay(1000)
        assertEquals(2, postsViewModel.commentsCount.value)
    }

    @Test
    fun testCommentsCountWhenOffline() = runBlockingTest {
        `when`(commentsDao.getComments()).thenReturn(
            arrayListOf(
                CommentEntity(
                    1,
                    1,
                    "asadd",
                    "hfh@df.com",
                    "fdgg"
                )
            )
        )
        `when`(networkConnectivity.isOnline(context)).thenReturn(false)
        withContext(Dispatchers.Unconfined) {
            postsViewModel.retrieveCommentsCount(1, networkConnectivity, context)
        }
        verify(commentsDao, times(1)).getComments()
        delay(1000)
        assertEquals(1, postsViewModel.commentsCount.value)
    }

    @Throws(Exception::class)
    fun readJsonFile(filename: String): String? {
        val basePath = "src/test/resources/com.sachinda.ceylonsolutionsproject/"
        val br =
            BufferedReader(InputStreamReader(FileInputStream(basePath + filename)))
        val sb = StringBuilder()
        var line: String? = br.readLine()
        while (line != null) {
            sb.append(line)
            line = br.readLine()
        }
        return sb.toString()
    }
}
